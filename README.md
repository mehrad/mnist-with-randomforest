# MNIST with RandomForest

This repo is just to share a quick project we did as a final project of a machine learning course. The task was to have a digit classification method that works well on [MNIST](https://en.wikipedia.org/wiki/MNIST_database) data.


## File Descriptions

The easiest way to see the report is by downloading the `report.pdf`, but it does not contain the code and it only has descriptions and figures. Alternatively, you can see the final report and most of the code in the HTML file (you can enable viewing code from a button in top-right corner).

If you want to run the code yourself, use the `final.Rmd`.


## Authors

- Mehrad Mahmoudian
- Thomas Faux

## License

This work is licensed under the Creative Commons Attribution 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by/4.0/ or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a>
