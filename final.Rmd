---
title: "TKO5519 Exercise, recognition of handwritten characters"
output: html_notebook
---

> Authors:
>
>    * Mehrad Mahmoudian - 506636 UTU
>
>    * Thomas Faux - 514466 UTU 

## Introduction

Such OCR problem is by nature a classification problem with random guess chance of 10%. In the following few pages we try to find our way through the given dataset and problem and the goal is to have predictability much higher than 10%.

## Read data and basic exploration

The given zip file (mnist_all.zip) contains two different sets of files, train and test, which each of them have 10 different files numbered from 0 to 9 which indicate the number they represent.

At this point we read data into R and into correct train and set sets:
```{r}
## create empty variables to fill iteratively
train_set <- c()
train_info <- c()
test_set <- c()
test_info <- c()

# iterate through 0 to 9
for (i in 0:9) {
    ## read i_th training data and append the lable
    tmp_train <- read.table(file = paste0("Data/mnist_all/train", i, ".txt"))
    train_set <- rbind.data.frame(train_set, tmp_train)
    train_info <- c(train_info, rep(i, nrow(tmp_train)))
    
    ## read i_th test data and append the lable
    tmp_test <- read.table(file = paste0("Data/mnist_all/test", i, ".txt"))
    test_set <- rbind.data.frame(test_set, tmp_test)
    test_info <- c(test_info, rep(i, nrow(tmp_test)))
    
    # Show user the progress
    # cat("\r", i)
}

# remove unnecessary variables
rm(tmp_test, tmp_train)

## define a function to visualize the digit of a specific row
display_digit <- function(row, main = NULL){
    image(t(apply(matrix(as.numeric(row), nrow = 28, byrow = T), 2, rev)), col = grey.colors(255), main = main)
}

# display 14th row (should be 0 based on the label)
display_digit(train_set[42000, ])
```

since we have 28x28 pixel picture and the written digit should be in this area, it would be somewhat a good predictive feature to know how much a digit is filling the area of this square. Logically 1 should take less area compared to 3, 5 and 8.

```{r}
train_info <- cbind.data.frame(train_info, apply(train_set, 1, mean))
colnames(train_info) <- c("label", "row.mean")
```

```{r}
# coverage_mean <- list()
plot(NULL, xlim = c(min(train_info$row.mean), max(train_info$row.mean)),
     ylim = c(0, 0.08), ylab = "Frequency")
for (i in 0:9) {
    # coverage_mean <- c(coverage_mean, list(density(train_info[train_info$label == i, "row.mean"])))
    lines(density(train_info[train_info$label == i, "row.mean"]), col = i + 1, lty = i + 1)
}

legend("topright", lty = 1:10, col = 1:10, legend = as.character(0:9))
# names(coverage_mean) <- 0:9


```

This plot shows that the average pixel intensity can be a useful feature and supports the hypothesis that for example number 1 can cover less area than 8 or 0. In spite of this, it cannot differentiate between 2, 3, 6 and 8 much.


## Machine learning

At this point it can be a good idea to just throw the data at a machine learning method and see how it performs. In the exercise material several methods have been mentioned but the only thing that we should recognize is that this question is by definition a classification problem. Based on data we know we have 784 features (columns) and 60k samples (rows), hence we should use a method that can handle relatively high dimensional data and can use massive sample size.

Based on the criteria above, intuitively, our best methods are either Randomforest or NeuralNetwork. The former needs less coding and tuning for initial run compared to latter. We will start with Randomforest an based on the result we will decide if we need to use NeuralNetwork.

### Random forest

For this reason, among all Randomforest packages out there we chose randomForestSRC since:
1. It is the most updated and the most actively developed one.
2. It can handle learning in multithread and also MPI form (we might need to use it if training gets lengthy)

```{r}
suppressPackageStartupMessages({
    # the RandomForest package
    library(randomForestSRC)
    # a package to assess our performance with ease
    library(MLmetrics)
})
# training with specific seed (for reproducability)
rfsrc.fit <- rfsrc(formula = label ~ .,
                   data = data.frame(label = factor(train_info$label), train_set),
                   seed = 1234,
                   do.trace = 5)
# predicting over test set
rfsrc.pred <- predict.rfsrc(object = rfsrc.fit, newdata = test_set)
```
Now it's the moment of truth:
```{r}
MLmetrics::ConfusionMatrix(y_pred = rfsrc.pred$class, y_true = test_info)
```
The confusion matrix above clearly shows that we have got an amazing model and consequently a very good result. We can turn this into accuracy:
```{r}
MLmetrics::Accuracy(y_pred = rfsrc.pred$class, y_true = test_info)
```

This is a very very good result considering that we are predicting on a test set that was not involved in learning process, hence a true validation set, with enough sample size.

We can interpret the confusion matrix further:
```{r}
colSums(matrix(rfsrc.conf_matrix[upper.tri(rfsrc.conf_matrix, diag = F) | lower.tri(rfsrc.conf_matrix, diag = F)], nrow = 9, byrow = F, dimnames =  list(c(1:9), as.character(0:9))))
```

We can clearly see that the troubling classes are "9" and "8" which have been miss-classified 43 and 42 times respectively. "8" has been mostly miss-classified as "3", and "9" has been mostly miss-classified as "4".



#### feature extraction/selection

Despite the fact that randomforest can return feature importance, we can try to reduce the feature space with some basic logic at this point. Logically the columns that are "mostly" empty are those with less information. 

For this reason we can count the number of non-zero values for each column (read pixel):
```{r}
barplot(sort(apply(train_set, 2, function(tmp_col) sum(tmp_col != 0))), col = "gray", border = NA)
# abline(h = sum((apply(train_set, 2, function(tmp_col) sum(tmp_col != 0))) == 0), col = "blue")
```
We can see how many of columns does not include any non-zero character:
```{r}
sum((apply(train_set, 2, function(tmp_col) sum(tmp_col != 0))) == 0)
```

We can definitely remove these columns since they does not contain any signal. This will give us 8.54 % reduction in feature number. We can also push this further and define a rational cutoff value. For example we can define that columns with non-zero values less than 10% of the population of a class with least samples size does not contain enough signal for us (basically we should find a balance between information gain and number of features) :
```{r}
sort(table(train_info$label))
```

our smallest samples size is for class "5" and 10% of it's sample size is roughly 542, hence:
```{r}
sum((apply(train_set, 2, function(tmp_col) sum(tmp_col != 0))) < 542)
```

This will provide 36.60% reduction in number of features (more than one third)

We now can remove these features and do another round of training/prediction and see how much this reduction has affected our accuracy:

```{r}
nonsignificant_features <- which((apply(train_set, 2, function(tmp_col) sum(tmp_col != 0))) < 542)
# training with specific seed (for reproducability)
rfsrc.fit2 <- rfsrc(formula = label ~ .,
                   data = data.frame(label = factor(train_info$label),
                                     train_set[, -nonsignificant_features]),
                   seed = 1234,
                   do.trace = 10)
# predicting over test set
rfsrc.pred2 <- predict.rfsrc(object = rfsrc.fit2, newdata = test_set[, -nonsignificant_features])

MLmetrics::ConfusionMatrix(y_pred = rfsrc.pred2$class, y_true = test_info)
```

```{r}
MLmetrics::Accuracy(y_pred = rfsrc.pred2$class, y_true = test_info)
```

It is clear that the primitive and yet logical feature selection didn't cost us accuracy and even the general trend in the confusion matrix didn't change.


#### Examples of failed classifications

```{r}
examples <- c()
for (i in 0:9) {
    tmp_mask <- test_info == i
    tmp_real_index <- min(which(tmp_mask)) + min(which(test_info[tmp_mask] != rfsrc.pred2$class[tmp_mask])) - 1
    examples <- c(examples, tmp_real_index)
}
names(examples) <- as.character(0:9)
```

```{r}
par(mfrow = c(2, 2))
for (i in 0:3) {
    display_digit(test_set[examples[as.character(i)], ],
                  main = paste("Real:", i,
                               "\tPredicted:", rfsrc.pred2$class[examples[as.character(i)]]))
}
```
```{r}
par(mfrow = c(2, 2))
for (i in 4:7) {
    display_digit(test_set[examples[as.character(i)], ],
                  main = paste("Real:", i,
                               "\tPredicted:", rfsrc.pred2$class[examples[as.character(i)]]))
}
```

```{r}
par(mfrow = c(1, 2))
for (i in 8:9) {
    display_digit(test_set[examples[as.character(i)], ],
                  main = paste("Real:", i,
                               "\tPredicted:", rfsrc.pred2$class[examples[as.character(i)]]))
}
```

#### Crossvalidation
"In random forests, there is no need for cross-validation or a separate test set to get an unbiased estimate of the test set error." [1] 



## Conclusion

We used Randomforest and a basic feature elimination to achieve a good predictivity. Randomforest helped us to create loads of decision trees and used it's ensembl capacity to have the final predictive model.





[1]:(https://www.stat.berkeley.edu/~breiman/RandomForests/cc_home.htm#ooberr)
